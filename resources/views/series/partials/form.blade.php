@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
@endsection

<div class="Form-element{{ $errors->has('name') ? ' u-error' : '' }}">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null,
        [
            'placeholder' => 'Nombre de la serie',
            'autocomplete' => 'off',
            'required',
            'maxlength' => 255,
            'class' => 'form-control'
        ])
    !!}
</div>
@if($errors->has('name'))
    <div class="Form-message  u-alert">
        <strong>{{ $errors->first('name') }}</strong>
    </div>
@endif
<div class="Form-element  u-flex-center{{ $errors->has('picture') ? ' u-error' : '' }}">
    <label for="picture">Cambiar foto</label>
    <label class="fileContainer  u-small">
        Examinar
        <input type="file" name="picture" id="picture">
    </label>
    <!-- {!! Form::label('picture', 'Imagen') !!}

    {!! Form::file('picture',
        ['class' => 'form-control']
    ) !!} -->
</div>
@if($errors->has('picture'))
    <div class="Form-message  u-alert">
        <strong>{{ $errors->first('picture') }}</strong>
    </div>
@endif
<div class="Form-element{{ $errors->has('information') ? ' u-error' : '' }}">
    {!! Form::label('information', 'Información:') !!}

    {!! Form::textarea('information',
        null,
        [
            'required',
            'placeholder' => 'Información',
            'class' => ''
        ]
    ) !!}
</div>
@if($errors->has('information'))
    <div class="Form-message  u-alert">
        <strong>{{ $errors->first('information') }}</strong>
    </div>
@endif
<div class="Form-element{{ $errors->has('trailer_url') ? ' u-error' : '' }}">
    {!! Form::label('trailer_url', 'Trailer:') !!}

    {!! Form::textarea('trailer_url',
        null,
        [
            'required',
            'placeholder' => 'Trailer',
            'maxlength' => 1024,
            'class' => ''
        ]
    ) !!}
</div>
@if($errors->has('trailer_url'))
    <div class="Form-message  u-alert">
        <strong>{{ $errors->first('trailer_url') }}</strong>
    </div>
@endif
<div class="Form-element{{ $errors->has('tags') ? ' u-error' : '' }}">
    {!! Form::label('tags', 'Tags:') !!}

    {!! Form::select('tags[]',
        $tags,
        null,
        [
            'multiple',
            'id' => 'tags',
            'required',
            'class' => 'form-control'
        ]
    ) !!}
</div>
@if($errors->has('tags'))
    <div class="Form-message  u-alert">
        <strong>{{ $errors->first('tags') }}</strong>
    </div>
@endif
<div class="Form-element  u-bg-success  u-lg-w35">
    <input type="submit" value="Guardar Cambios">
</div>

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('#tags').select2({
                placeholder: 'Seleccione uno o más tags',
                tags: true,
                tokenSeparators: [',']
            });
        }); /* Fin de jQuery */
    </script>

@endsection
